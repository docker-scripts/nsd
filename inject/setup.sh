#!/bin/bash -x

source /host/settings.sh

# create 'notify' and 'provide-xfr' config files
echo '' > /host/config/notify.conf
echo '' > /host/config/provide-xfr.conf
for server_ip in $AXFR_SERVERS; do
    echo "notify: $server_ip NOKEY" >> /host/config/notify.conf
    echo "provide-xfr: $server_ip NOKEY" >> /host/config/provide-xfr.conf
done
echo 'provide-xfr: 127.0.0.1 NOKEY' >> /host/config/provide-xfr.conf

# create 'secondary.ns' include file
echo '' > config/secondary.ns
for ns in $SECONDARY_NS; do
    echo "@  IN  NS  ${ns}." >> /host/config/secondary.ns
done

# create the config file '/etc/nsd/nsd.conf'
echo 'include: /host/config/nsd.conf' > /etc/nsd/nsd.conf

# add ufw rules for allowing axfr servers
sed -i /etc/default/ufw \
    -e '/^IPV6=/ c IPV6=no'
for server_ip in $AXFR_SERVERS; do
    ufw allow from $server_ip to any port 53
done
ufw enable
