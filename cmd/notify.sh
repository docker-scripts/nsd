cmd_notify_help() {
    cat <<_EOF
    notify
        Notify secondary (slave) DNS servers to refetch AXFR data.

_EOF
}

cmd_notify() {
    ds exec nsd-control reload
    ds exec nsd-control notify
    ds shell tail /var/log/syslog -f
}
