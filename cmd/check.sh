cmd_check_help() {
    cat <<_EOF
    check [ -z, --zones | -c, --conf ]
        Check zone files or configuration.

_EOF
}

cmd_check() {
    case $1 in
        -z|--zones)
            for zone_file in $(ls zones/*.db); do
                local zone=$(echo $zone_file | sed -e 's#^zones/##' -e 's#\.db$##')
                ds exec nsd-checkzone $zone $zone_file
            done
            ;;
        -c|--conf)
            ds exec nsd-checkconf -v /etc/nsd/nsd.conf
            ;;
        *)
            fail "Usage:\n$(cmd_check_help)"
            ;;
    esac
}
