rename_function cmd_create orig_cmd_create
cmd_create() {
    chmod +x .
    orig_cmd_create \
        --publish $(get_public_ip):53:53 \
        --cap-add=NET_ADMIN
    # NET_ADMIN needed for iptables
}
