cmd_config() {
    ds inject msmtp.sh
    ds inject logwatch.sh $(hostname)

    # disable ipv6
    sysctl -w net.ipv6.conf.all.disable_ipv6=1
    sysctl -w net.ipv6.conf.default.disable_ipv6=1

    ds inject setup.sh
}
