#!/bin/bash

# copy example zone db
mkdir -p zones/
cp $SCRIPTS/nsd/misc/example.org.db zones/

# fix the ip on the example zone file
sed -i zones/example.org.db \
    -e "s/127\.0\.0\.1/$(get_public_ip)/"

# create nsd config file
mkdir -p config
cat <<EOF > config/nsd.conf
server:
    do-ip6: no
    zonesdir: /host/zones
    verbosity: 2

pattern:
    name: axfr-servers
    include: /host/config/notify.conf
    include: /host/config/provide-xfr.conf

include: /host/zones/*.zone
EOF

#cp $SCRIPTS/nsd/misc/googlehosted config/
#cp $SCRIPTS/nsd/misc/google.mx config/
