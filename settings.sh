APP=nsd

SECONDARY_NS="
    $(: https://www.buddyns.com/support/setup/zone-delegation/free/ )
    uz5qfm8n244kn4qz8mh437w9kzvpudduwyldp5361v9n0vh8sx5ucu.free.ns.buddyns.com    $(: 'USA, California' )
    uz5x36jqv06q5yulzwcblfzcrk1b479xdttdm1nrgfglzs57bmctl8.free.ns.buddyns.com    $(: 'Germany, EU' )

    ns0.1984.is
    $(: ns1.1984.is )
    $(: ns2.1984.is )
    $(: ns1.1984hosting.com )
    $(: ns2.1984hosting.com )

    $(: https://puck.nether.net/dns/static/faq.html )
    puck.nether.net

    $(: https://freedns.afraid.org/secondary/instructions.php )
    ns2.afraid.org
"
AXFR_SERVERS="
    $(: https://www.buddyns.com/support/setup/zone-transfer/free/ )
    108.61.224.67
    116.203.6.3
    107.191.99.111
    185.22.172.112
    103.6.87.125
    192.184.93.99
    119.252.20.56
    31.220.30.73
    185.34.136.178
    185.136.176.247
    45.77.29.133
    116.203.0.64
    167.88.161.228
    199.195.249.208
    104.244.78.122

    $(: '1984hosting.com' )
    93.95.224.6

    $(: 'puck.nether.net' )
    204.42.254.5

    $(: 'freedns.afraid.org' )
    69.65.50.192
"
