cmd_zone_help() {
    cat <<_EOF
    zone ( add | rm | en | dis | test ) <domain>
        Manage domain zones.

_EOF
}

cmd_zone() {
    local do=$1
    local domain=$2
    [[ -n $domain ]] || fail "Usage:\n$(cmd_zone_help)"

    case $do in
        add|rm|dis|en)
            _zone_$do $domain
            ds exec systemctl reload nsd
            ;;
        test)
            [[ -f zones/$domain.db ]] \
                || fail "File 'zones/$domain.db' does not exist"
            _zone_$do $domain
            ;;
        *)
            fail "Usage:\n$(cmd_zone_help)"
            ;;
    esac
}

_zone_add() {
    local domain=$1

    cat <<EOF > zones/$domain.zone
zone:
    name: $domain
    zonefile: $domain.db
    include-pattern: "axfr-servers"
EOF

    cp zones/{example.org.db,$domain.db}
    sed -i zones/$domain.db -e "s/example.org/$domain/g"
}

_zone_rm() {
    local domain=$1
    rm zones/$domain.*
}

_zone_dis() {
    local domain=$1
    mv zones/$domain.zone{,.disabled}  
}

_zone_en() {
    local domain=$1
    mv zones/$domain.zone{.disabled,}  
}

_zone_test() {
    local domain=$1
    set -x
    ds exec dig @localhost axfr $domain 
}
