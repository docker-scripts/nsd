# NSD

This is a **hidden**, **master**, **authoritative-only** name-server,
which helps you to manage yourself your domains.

**Hidden** means that it stays behind a firewall, not accessible from
the outside world. **Master** or **primary** means that it is a
primary source of information for the domains that it provides. There
are also **slave/secondary** DNS servers, which get the information of
the domains that they cover from other (master/primary) servers. If we
update the domain on a master server, the slaves will synchronise with
it automatically after a certain time.

**Authoritative-only** means that the server will just give answers
for the domains that it masters, and nothing else. DNS servers can
possibly do several things, for example give answers to DNS requests
from clients, both for the domains that they are responsible for and
for other domains. If they don't know the answer, they get it from the
Internet, fetch it to the client and then cache it for future
requests. However this server does not do any of these things. It just
answers for its own domains.

See also this: [How to Manage Your Own Domain Name
Server](http://dashohoxha.fs.al/howto-manage-your-own-nameserver/)


## Installation

  - First install `ds`:
     + https://gitlab.com/docker-scripts/ds#installation

  - Then get the scripts: `ds pull nsd`

  - Create a directory for the container: `ds init nsd @nsd`

  - Fix the settings: `cd /var/ds/nsd/ ; vim settings.sh`

  - Make the container: `ds make`

**Note:** Usually port `53` is in use by `systemd-resolved` (you can
check it with `lsof -i :53`). You have to stop and disable it first
and run `ds make` again:

```
systemctl stop systemd-resolved
systemctl disable systemd-resolved
systemctl mask systemd-resolved

rm /etc/resolv.conf
cat << EOF > /etc/resolv.conf
nameserver 8.8.8.8
nameserver 9.9.9.9
EOF

lsof -i :53

ds make
```

## Usage

```
ds zone ( add | rm | en | dis | test ) <domain>
    Manage domain zones.
```

```
ds check [ -z, --zones | -c, --conf ]
    Check zone files or configuration.
```

```
ds notify
    Notify secondary (slave) DNS servers to refetch AXFR data.
```

## Troubeshooting

```
cd /var/ds/nsd/
ds shell
service nsd restart
service nsd status
tail /var/log/syslog -n 30
dig @localhost axfr example.org
ufw status
```
